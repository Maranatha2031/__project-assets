//This class is auto-generated do not modify
namespace GameFramework
{
	public static class Resources
	{
		public const string Style1 = "Style 1";
		public const string Style2 = "Style 2";
		public const string Style3 = "Style 3";
		public const string Style6 = "Style 6";
		public const string Style7 = "Style 7";
		public const string Style8 = "Style 8";
		public const string Style10 = "Style 10";
		public const string Style11 = "Style 11";
		public const string Style4 = "Style 4";
		public const string Style5 = "Style 5";
		public const string Style9 = "Style 9";
		public const string DOTweenSettings = "DOTweenSettings";
		public const string GameFoundationSettings = "GameFoundationSettings";
		public const string AppSettings = "AppSettings";
		public const string GameFrameworkSettings = "GameFrameworkSettings";
	}
}