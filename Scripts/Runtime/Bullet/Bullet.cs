﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
    private SpriteRenderer _spriteRen;
    private Rigidbody2D _rb;

    public SpriteRenderer SpriteRen => _spriteRen;

    void Awake()
    {
        _spriteRen = GetComponent<SpriteRenderer>();
        _rb = GetComponent<Rigidbody2D>();
    }

    public void SetVelocity(float speed)
    {
        _rb.velocity = Vector2.left * speed;
    }
}
