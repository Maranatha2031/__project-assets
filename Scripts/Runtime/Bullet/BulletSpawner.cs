﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    public bool startSpawningAtStart = true;

    public Bullet[] prefabs;
    public float timeBetweenSpawns = 0.77f;
    public float bulletSpeed = 4f;

    private WaitForSeconds _waitToSpawn;
    Coroutine _infiniteRoutine;

    private Bullet RandomBullet => prefabs[Random.Range(0, prefabs.Length)];

    private void Start()
    {
        if (startSpawningAtStart)
            StartSpawn();
    }

    public void StartSpawn()
    {
        if (_infiniteRoutine == null)
            _infiniteRoutine = StartCoroutine(SpawnCo());
    }

    public void StopSpawn()
    {
        if (_infiniteRoutine != null)
        {
            StopCoroutine(_infiniteRoutine);
            _infiniteRoutine = null;
        }
    }

    private IEnumerator SpawnCo()
    {
        _waitToSpawn = new WaitForSeconds(timeBetweenSpawns);

        while(true)
        {
            Spawn();
            yield return _waitToSpawn;
        }
    }

    private void Spawn()
    {
        var instance = Instantiate(RandomBullet, transform.position, transform.rotation);
        instance.SetVelocity(bulletSpeed);
    }
}
