﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using d4160.Core;
using d4160.GameFramework;
using UnityEngine.GameFoundation;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class PlayerController : MonoBehaviour
{
    public Sprite[] shapes;
    public int maxHealth = 3;
    public int scoreObjective = 10;
    [Header("UI")]
    public Text scoreText;
    public Text healthText;
    public GameObject loseScreen;
    public GameObject winScreen;
    public GameObject playFlowCanvas;
    [Header("SETTINGS")]
    public bool enableInputAtStart = true;
    public bool restartAutomaticallyAfterLose = true;
    //[DeConditional("restartAutomaticallyAfterLose", true, ConditionalBehaviour.Hide)]
    public float timeToWaitUntilRestart = 1.15f;
    //[DeConditional("restartAutomaticallyAfterLose", false, ConditionalBehaviour.Hide)]

    private int _score;
    private int _currentHealth;
    private bool _inputEnabled;
    private int _gameMode = 0;

    private SpriteRenderer _spriteRen;
    private Collider2D _col;
    private DefaultPlayLauncher _gameModeLauncher;

    public int GameMode
    {
        get => _gameMode;
        set
        {
            _gameMode = value;

            _gameModeLauncher = GameManager.Instance.GetPlayLevelLauncher<DefaultPlayLauncher>(_gameMode);
        }
    }

    public int Score => _score;

    private void Awake()
    {
        _spriteRen = GetComponent<SpriteRenderer>();
        _col = GetComponent<Collider2D>();
    }

    private void Start()
    {
        InitializeValues();

        InitializeUI();

        InitializeInput();

        SetEnableColliders(true);

        SetEnableRenderers(true);
    }

    private void Update()
    {
        if (!_inputEnabled) return;

        var k = Keyboard.current;
        if (k.cKey.wasPressedThisFrame)
        {
            ChangeShape(0);
        }
        else if(k.sKey.wasPressedThisFrame)
        {
            ChangeShape(1);
        }
        else if (k.tKey.wasPressedThisFrame)
        {
            ChangeShape(2);
        }
    }

    public void ChangeShape(int index)
    {
        if (shapes.IsValidIndex(index))
            _spriteRen.sprite = shapes[index];
    }

    public void Restart()
    {
        Start();
    }

    public void SetEnableInput(bool enabled)
    {
        _inputEnabled = enabled;
    }

    public void SetEnableRenderers(bool enabled)
    {
        _spriteRen.enabled = enabled;
    }

    public void SetEnableColliders(bool enabled)
    {
        _col.enabled = enabled;
    }

    public void SetInteractive(bool interactive)
    {
        SetEnableInput(interactive);
        SetEnableRenderers(interactive);
        SetEnableColliders(interactive);
    }

    private void InitializeUI()
    {
        if (scoreText)
            scoreText.text = $"Score: {_score}";

        if (healthText)
            healthText.text = $"Health: {_currentHealth}";

        if (loseScreen) loseScreen.SetActive(false);
        if (winScreen) winScreen.SetActive(false);
    }

    private void InitializeValues()
    {
        _currentHealth = maxHealth;
        _score = 0;
    }

    private void InitializeInput()
    {
        SetEnableInput(enableInputAtStart);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var bullet = other.GetComponent<Bullet>();
        if (bullet)
        {
            if (bullet.SpriteRen.sprite == _spriteRen.sprite)
            {
                _score++;
                if (scoreText)
                    scoreText.text = $"Score: {_score}/{scoreObjective}";

                CheckObjectiveComplete();
            }
            else
            {
                if (_gameModeLauncher && _gameModeLauncher.PlayState == PlayState.GameOver)
                {
                    Destroy(other.gameObject);
                    return;
                }

                _currentHealth--;

                CheckDefeat();

                if (healthText)
                    healthText.text = $"Health: {_currentHealth}";
            }

            Destroy(other.gameObject);
        }
    }

    private void RestartPlay()
    {
        GameManager.Instance.RestartPlay(false, _gameMode);
    }

    private void CheckDefeat()
    {
        if (_currentHealth <= 0)
        {
            _currentHealth = 0;

            SetInteractive(false);

            if (restartAutomaticallyAfterLose)
            {
                Invoke("RestartPlay", timeToWaitUntilRestart);
            }
            else
            {
                if (loseScreen) loseScreen.SetActive(true);

                GameManager.Instance.SetGameOver(PlayResult.Lose, _gameMode);
            }

            if (playFlowCanvas) playFlowCanvas.SetActive(false);
        }
    }

    private void StoreScore()
    {
        InventoryItem trial = Inventory.main.GetItem("normalModePlayerStats");
        trial.SetStatInt("highScore", _score);
        //Inventory.main.SetStatInt("score", _score);
        //StatManager.SetIntValue(trial, );

    }

    public void SubmitScore(int playerScore) {
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest {
            Statistics = new List<StatisticUpdate> {
                new StatisticUpdate {
                    StatisticName = "HighScore2",
                    Value = playerScore
                }
            }
        }, result=> OnStatisticsUpdated(result), FailureCallback);
    }

    private void OnStatisticsUpdated(UpdatePlayerStatisticsResult updateResult) {
        Debug.Log("Successfully submitted high score");
    }

    private void FailureCallback(PlayFabError error){
        Debug.LogWarning("Something went wrong with your API call. Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }

    private void CheckObjectiveComplete()
    {
        if (_gameModeLauncher && _gameModeLauncher.PlayState == PlayState.GameOver)
        {
            return;
        }

        if (_score >=  scoreObjective)
        {
            if (winScreen) winScreen.SetActive(true);

            GameManager.Instance.SetGameOver(PlayResult.Win, _gameMode);

            if (playFlowCanvas) playFlowCanvas.SetActive(false);
        }
    }
}