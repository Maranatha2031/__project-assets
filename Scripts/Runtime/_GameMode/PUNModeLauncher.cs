﻿namespace GameFramework
{
    using d4160.GameFramework;
    using UnityEngine;
    using UnityEngine.UI;
    using Photon.Pun;
    using Photon.Pun.Demo.PunBasics;
    using Photon.Realtime;
    using System;

    public class PUNModeLauncher : DefaultPUNPlayLauncher
    {
        [Tooltip("The prefab to use for representing the player")]
        public GameObject playerPrefab;
        public bool startPlayingAutomatically;
        //[DeConditional("startPlayingAutomatically", true, ConditionalBehaviour.Hide)]
        public float delayToStartPlaying;

        private Text _text;
        private Button _leaveRoomButton;

        private float _playingTime;

        public override void OnEnable()
        {
            base.OnEnable();

            PhotonNetwork.OnSyncLevelLoad += OnSyncLevelLoad;
        }

        public override void OnDisable()
        {
            base.OnDisable();

            PhotonNetwork.OnSyncLevelLoad -= OnSyncLevelLoad;
        }

        private void OnSyncLevelLoad(int sceneBuildIndex)
        {
            d4160.GameFramework.GameManager.Instance.UnloadLevel(LevelType.GameMode, 2, () => {

                if (d4160.GameFramework.GameManager.Instance.GetPlayLevelLauncher(2) is DefaultPlayLauncher)
                {
                    var chapter = (d4160.GameFramework.GameManager.Instance.GetPlayLevelLauncher(2) as DefaultPlayLauncher).CurrentChapter;
                    chapter.LevelScene = new LevelScene() { levelCategory = 6, levelScene = PhotonNetwork.CurrentRoom.PlayerCount };
                }

                d4160.GameFramework.GameManager.Instance.LoadLevel(LevelType.GameMode, 2);
            });
        }

        public override void SetReadyToPlay()
        {
            Debug.LogWarning($"Multiplayer SetReadyToPlay {!PhotonNetwork.IsConnected}");
            // in case we started this demo with the wrong scene being active, simply load the menu scene
			if (!PhotonNetwork.IsConnected)
			{
				d4160.GameFramework.GameManager.Instance.UnloadLevel(LevelType.GameMode, 2, () => {
                    d4160.GameFramework.GameManager.Instance.LoadLevel(LevelType.General, 1);
                });

				return;
			}

            _text = GameObject.FindGameObjectWithTag("TimeText")?.GetComponent<Text>();
            _leaveRoomButton = GameObject.FindGameObjectWithTag("LeaveRoomButton")?.GetComponent<Button>();

            _leaveRoomButton?.onClick.AddListener(LeaveRoom);

            base.SetReadyToPlay();

            _playingTime = 0f;
            UpdatePlayingTimeText();

            if (playerPrefab == null)
            { // #Tip Never assume public properties of Components are filled up properly, always check and inform the developer of it.

				Debug.LogError("<Color=Red><b>Missing</b></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
			}
            else
            {
				if (PlayerManager.LocalPlayerInstance==null)
				{
				    Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);

					// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
					PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f,5f,0f), Quaternion.identity, 0);
				}else{

					Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
				}
			}

            Camera.main.orthographic = false;

            if (startPlayingAutomatically)
                Invoke("Play", delayToStartPlaying);
        }

        public override void Play()
        {
            base.Play();
        }

        public override void SetGameOver(PlayResult result)
        {
            // TODO Update stats

            base.SetGameOver(result);
        }

        void Update()
        {
            if (m_playState == PlayState.Playing)
            {
                _playingTime += Time.deltaTime;

                UpdatePlayingTimeText();
            }
        }

        void UpdatePlayingTimeText()
        {
            if (_text)
                _text.text = $"{(int)_playingTime}";
        }

        /// <summary>
        /// Called when a Photon Player got connected. We need to then load a bigger scene.
        /// </summary>
        /// <param name="other">Other.</param>
        public override void OnPlayerEnteredRoom(Player other  )
		{
            //if (IgnoreCallbacks) return;

			Debug.Log( "OnPlayerEnteredRoom() " + other.NickName); // not seen if you're the player connecting

			if ( PhotonNetwork.IsMasterClient )
			{
				Debug.LogFormat( "OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient ); // called before OnPlayerLeftRoom

				LoadArena();
			}
		}

        /// <summary>
		/// Called when a Photon Player got disconnected. We need to load a smaller scene.
		/// </summary>
		/// <param name="other">Other.</param>
		public override void OnPlayerLeftRoom( Player other  )
		{
            //if (IgnoreCallbacks) return;

			Debug.Log( "OnPlayerLeftRoom() " + other.NickName ); // seen when other disconnects

			if ( PhotonNetwork.IsMasterClient )
			{
				Debug.LogFormat( "OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient ); // called before OnPlayerLeftRoom

				LoadArena();
			}
		}

        /// <summary>
		/// Called when the local player left the room. We need to load the launcher scene.
		/// </summary>
		public override void OnLeftRoom()
		{
            //if (IgnoreCallbacks) return;

            //Debug.LogWarning( "OnLeftRoom() " );
            Camera.main.orthographic = true;
            Camera.main.transform.SetPositionAndRotation(Vector3.back * 10, Quaternion.identity);

            d4160.GameFramework.GameManager.Instance.UnloadLevel(LevelType.GameMode, 2, () => {
                d4160.GameFramework.GameManager.Instance.LoadLevel(LevelType.General, 1);
            });

            //IgnoreCallbacks = true;
		}

        public void LeaveRoom()
		{
            //Debug.LogWarning( "LeaveRoom() " );

			PhotonNetwork.LeaveRoom();
		}

        #region Private Methods

		void LoadArena()
		{
			if ( ! PhotonNetwork.IsMasterClient )
			{
				Debug.LogError( "PhotonNetwork : Trying to Load a level but we are not the master Client" );
			}

			Debug.LogFormat( "PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount );

			//PhotonNetwork.LoadLevel("PunBasics-Room for "+PhotonNetwork.CurrentRoom.PlayerCount, LoadSceneMode.Additive);

            d4160.GameFramework.GameManager.Instance.UnloadLevel(LevelType.GameMode, 2, () => {
                var chapter = CurrentChapter;
                chapter.LevelScene = new LevelScene() { levelCategory = 6, levelScene = PhotonNetwork.CurrentRoom.PlayerCount};
                d4160.GameFramework.GameManager.Instance.LoadLevel(LevelType.GameMode, 2);
            });
        }

		#endregion
    }
}