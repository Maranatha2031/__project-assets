﻿namespace GameFramework
{
    using d4160.GameFramework;
    using UnityEngine;
    using UnityEngine.UI;

    public class NormalModeLauncher : DefaultModeLauncher
    {
        public bool startPlayingAutomatically;
        //[DeConditional("startPlayingAutomatically", true, ConditionalBehaviour.Hide)]
        public float delayToStartPlaying;

        private BulletSpawner _spawner;
        private PlayerController _playerController;
        private Text _text;

        private float _playingTime;

        public override void SetReadyToPlay()
        {
            _spawner = GameObject.FindGameObjectWithTag("Spawner")?.GetComponent<BulletSpawner>();
            _playerController = GameObject.FindGameObjectWithTag("Player")?.GetComponent<PlayerController>();
            _text = GameObject.FindGameObjectWithTag("TimeText")?.GetComponent<Text>();

            base.SetReadyToPlay();

            if (_playerController)
            {
                _playerController.GameMode = 1;
            }

            _playingTime = 0f;
            UpdatePlayingTimeText();

            if (startPlayingAutomatically)
                Invoke("Play", delayToStartPlaying);
        }

        public override void Play()
        {
            if (_spawner)
                _spawner.StartSpawn();

            if (_playerController)
            {
                _playerController.SetEnableInput(true);
            }

            base.Play();
        }

        public override void SetGameOver(PlayResult result)
        {
            if (_spawner)
                _spawner.StopSpawn();

            // TODO Update stats

            base.SetGameOver(result);
        }

        void Update()
        {
            if (m_playState == PlayState.Playing)
            {
                _playingTime += Time.deltaTime;

                UpdatePlayingTimeText();
            }
        }

        void UpdatePlayingTimeText()
        {
            if (_text)
                _text.text = $"{(int)_playingTime}";
        }
    }
}