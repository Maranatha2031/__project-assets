﻿using UnityEngine;
using UnityEngine.AddressableAssets;

public class AssetLoader : MonoBehaviour
{
    public AssetReference cubeAsset;

    public async void LoadAsset()
    {
        var prefab = cubeAsset.LoadAssetAsync<GameObject>();
        await prefab.Task;

        //UberDebug.LogChannel("Addresables", $"Load \"{cubeAsset.Asset.name}\": Done?{prefab.IsDone}, null?{prefab.Result == null}");

        if (prefab.Result)
            Instantiate(prefab.Result);
    }
}
